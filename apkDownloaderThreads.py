#!/usr/bin/python3

import threading
import requests
import sys, os, argparse
import queue
import parse

q = queue.Queue()
foldername = ''
counter = 1
lock = threading.Lock()

def downloadAPK(package):
    global foldername, counter
    lock.acquire()
    num = counter
    counter += 1
    lock.release()
    packageName = package
    downloadLink = None
    downloadLink = parse.apkPure(packageName)
    if downloadLink is None:
        return False
        # apk-dl.com is blocked for now; work in progress.
        #downloadLink = parse.apkDl(packageName)
        #if downloadLink is None:
            #return False

    # Download the APK
    print("[",num,"]","Downloading", packageName,"...")
    try:
        apkDownload = requests.get(downloadLink)
        outFile = open(foldername+packageName+'.apk', 'wb')
        for chunk in apkDownload.iter_content(100000):
        	outFile.write(chunk)
        print("Completed download of", packageName)
    except requests.exceptions.RequestException as e:
        print("Request Exception: "+e)
        return False
    return True

def workerThread():
    while True:
        package = q.get()
        if package is None:
            break
        q.task_done()
        if downloadAPK(package) is False:
            print("[-] Cannot download "+package)
            #q.put(package)

def main():
    global q, foldername
    threads = []
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--directory", default = "apks/", dest="directory", help="A directory to save APK files.")
    parser.add_argument("-f", "--file", default = "packages.txt", dest="file", help="File containing package names to download")
    args = parser.parse_args()

    filename = args.file
    foldername = args.directory
    folderpath = os.path.realpath(foldername)
    if not os.path.isdir(folderpath):
        print("[-] Folder does not exist. Please check the path and usage.")
        exit(1)

    try:
        with open(filename, "rU") as f:
            # Init Queue of package name
            for line in f:
                line = line.split("\n")[0]
                q.put(line)

            # Init threads
            num_worker_threads = 5
            for i in range(num_worker_threads):
                t = threading.Thread(target=workerThread)
                t.start()
                threads.append(t)

            # Wait for all package names to be handled
            q.join()

            # Make sure queue is empty, and then wait for threads to end.
            for i in range(num_worker_threads):
                q.put(None)
            for t in threads:
                t.join()

    except IOError as e:
        print ("I/O error: %s: %s" % (filename, e.strerror))
        sys.exit(1)



if __name__ == '__main__':
    main()
