# Dependencies: requests, and BeautifulSoup4

import requests
import bs4
import sys

if(len(sys.argv) < 3):
	print("Not enough arguments.")
	sys.exit()

fileToOpen = sys.argv[1]
APKDirectory = sys.argv[2]

# Read all package names we wish to download
packages = (pName.rstrip('\n') for pName in open(fileToOpen))


for package in packages:
	packageName = package

	# Fetch search page 
	searchPage = requests.get('https://apkpure.com/search?q='+packageName)

	# Convert HTML into a parsable object using BeautifulSoup4
	# So we can navigate to the Apps page on apkpure.com.
	# Take the first result and extract the lhttps://apkpure.com/aliexpress-shopping-app/com.alibaba.aliexpresshd/download?from=detailsink to its page
	searchSoup = bs4.BeautifulSoup(searchPage.text, 'lxml')
	downloadResults = searchSoup.select('.search-dl')
	topResult = downloadResults[0]
	topResult = topResult.find('a')
	
	# Fetch app download page
	downloadPage = requests.get('https://apkpure.com'+topResult.get('href')+'/download?from=details')
	
	# Fetch the APK download link 
	downloadSoup = bs4.BeautifulSoup(downloadPage.text, 'lxml')
	downloadLink = downloadSoup.select('#download_link')
	downloadLink = downloadLink[0].get('href')
	
	# Download the APK
	print("Downloading", packageName,"...")
	apkDownload = requests.get(downloadLink)
	print("Completed download of", packageName)
	print()

	outFile = open(APKDirectory+packageName+'.apk', 'wb')

	for chunk in apkDownload.iter_content(100000):
		outFile.write(chunk)