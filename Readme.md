# APK Downloader

## Overview

APK Downloader is a tool to download APK files from given package names.

## Dependency and Installation

APK Downloader requires requests and beautifulsoup4. Here is a [document](http://docs.python-requests.org/en/master/) about Requests. Here is another [document](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) about beautifulsoup4. Please check those links if you need to install request and/or beautifulsoup4. 

## Usage

python3 apkDownloaderThreads.py [-d] <directory> [-f] <filename>

-d: Directory to store APK files (Default: apks/)

-f: File name contains package names separated by new lines (Default: package.txt)

## APK Repositories

Current version of APK downloader only crawls APKPure.com for downloading APKs. 

## Output

APK files under the given folder. 

## Sample Package Name File (package.txt)

Those package names are collected from Google Play Store Top 200 free lists, mostly from Top 100. We tested using package.txt to download all files and we can download all files except few files not on ApkPure.com.

# Update Logs

### Version 0.2

Now supports multi-threading to download maximum five APKs at the same time. 

### Version 0.1

Initial version of APK Downloader. Crawl APKPure.com for all packages in 'packages.txt', and download those APKs into the 'apks/' directory.

'packages.txt' should be a new line separated file. Separate all packages by new lines.