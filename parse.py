#!/usr/bin/python3

import requests
import bs4
import datetime

def apkPure(packageName):
    # Fetch search page
    searchPage = requests.get('https://apkpure.com/search?q='+packageName)

    # Convert HTML into a parsable object using BeautifulSoup4
    # So we can navigate to the Apps page on apkpure.com.
    # Take the first result and extract the lhttps://apkpure.com/aliexpress-shopping-app/com.alibaba.aliexpresshd/download?from=detailsink to its page
    searchSoup = bs4.BeautifulSoup(searchPage.text, 'lxml')
    downloadResults = searchSoup.select('.search-dl')
    try:
        topResult = downloadResults[0]
    except IndexError:
        return None
    topResult = topResult.find('a')

    # Fetch app download page
    downloadPage = requests.get('https://apkpure.com'+topResult.get('href')+'/download?from=details')

    # Fetch the APK download link
    downloadSoup = bs4.BeautifulSoup(downloadPage.text, 'lxml')
    downloadLink = downloadSoup.select('#download_link')
    if len(downloadLink) is 0:
        return None
    downloadLink = downloadLink[0].get('href')
    return downloadLink

def apkDl(packageName):
    # Path format
    # http://apkfind.com/root/apk/<YYYY/MM/DD>/<package>_<version>.apk?id=<packagename>&f=<filename>_<version>_apk-dl.com.apk&dl=1
    # Examples
    # http://apkfind.com/root/apk/2016/10/7/com.sgn.pandapop.gp_4007014.apk?id=com.sgn.pandapop.gp&f=Panda%20Pop_4.7.014_apk-dl.com.apk&dl=1
    # http://apkfind.com/root/apk/2016/9/25/com.pandora.android_76103.apk?id=com.pandora.android&f=Pandora® Radio_7.6.1_apk-dl.com.apk&dl=1
    # http://apkfind.com/root/apk/2016/9/25/com.pandora.android_76103.apk?id=com.pandora.android&f=Pandora%C2%AE%20Radio_7.6.1_apk-dl.com.apk&dl=1
    searchPage = requests.get('http://apk-dl.com/'+packageName)
    searchSoup = bs4.BeautifulSoup(searchPage.text, 'lxml')
    downloadResults = str(searchSoup.find_all("ul", class_="apks dlist")[0]).split("> <")[1]
    dateinfo = str(downloadResults.split("\"")[1])
    dateinfo_slash = dateinfo.replace("_","/")
    downloadResults = str(searchSoup.find_all("li", {"class":"mdl-menu__item"})[0]).split(",")
    downloadResults = str(downloadResults[0]).replace("(","===")
    downloadResults = downloadResults.replace(")","===")
    versioninfo = downloadResults.split("===")[1]
    downloadResults = str(searchSoup.find_all("section", {"class":"detail"})).split("\"")[5]
    nameversioninfo = downloadResults.split(" ")
    nameinfo = "%20".join(nameversioninfo[0:len(nameversioninfo)-1])
    nameversioninfo = str(nameversioninfo[len(nameversioninfo)-1])
    filename = nameinfo + "_" + nameversioninfo + "_apk-dl.com.apk&dl=1"
    fullpath = "http://apkfind.com/root/apk/"+dateinfo_slash+"/"+packageName+"_"+versioninfo+".apk?id="+packageName+"&f="+filename
    return fullpath


def month_converter(month):
    months = ['January', 'February', 'March', 'April', 'May', 'June', 'Julu', 'August', 'September', 'October', 'November', 'December']
    return str(months.index(month) + 1)
